import random
import string
password_length = input()
number_of_passwords = 10
if password_length != 0 and password_length.isdigit():
    for i in range(int(password_length)):
        for j in range(number_of_passwords):
            print(random.choice(string.ascii_letters + string.digits), end='')
        print()
