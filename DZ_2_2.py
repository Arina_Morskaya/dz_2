my_array = (input("Enter array: ").split())
value = input("Enter value: ")
first_element = 0
last_element = len(my_array) - 1
middle = len(my_array) // 2
while my_array[middle] != value and first_element <= last_element:
    if int(value) > int(my_array[middle]):
        first_element = middle + 1
    else:
        last_element = middle - 1
    middle = (first_element + last_element)//2
print('Index value "%s" - %d' % (value, middle))
